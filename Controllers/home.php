<?php

    class Home extends Controllers{
        public function _construct()
        {
            parent::__construct();
        }

        public function home($params)
        {
            echo "Home Controller";
        }

        public function data($params)
        {
            echo "Data diterima: ".$params;
        }

        public function cart($params)
        {
            $cart = $this->model->getCart($params);
            echo $cart;
        }
    }